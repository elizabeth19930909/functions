def fib(n):
    if n in [1,2]:
        return 1

    return fib(n-1)+fib(n-2)

n = int(raw_input('n = '))
print fib(n)

# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597

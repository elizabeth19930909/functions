def unique(L):
    return list(set(L))

L = [1, 'one', 4, 1, 'two', 5, 'one', 5, 6]
print unique(L)
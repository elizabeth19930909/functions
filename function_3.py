def pan(p):
    return not set('abcdefghijklmnopqrstuvwxyz') - set(p.lower())

p = raw_input()
#p = 'The quick brown fox jumps over the lazy dog'
print pan(p)